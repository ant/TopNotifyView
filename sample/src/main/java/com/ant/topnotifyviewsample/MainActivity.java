package com.ant.topnotifyviewsample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ant.topnotifyview.TopNiftyView;

public class MainActivity extends AppCompatActivity {
    Button bt;
    private SeekBar seekBar;
    private TextView tvProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt = (Button) findViewById(R.id.bt_show);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        tvProgress = (TextView) findViewById(R.id.tv_progress);
        tvProgress.setText("MarginTop:" + seekBar.getProgress());

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvProgress.setText("MarginTop:" + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TopNiftyView.make(MainActivity.this, "That's pretty cool aha~", TopNiftyView.LENGTH_SHORT, seekBar.getProgress())
                        .setDuration(1500)
                        .setBackground(0xff4dc64a)
                        .setTextColor(0xffffffff)
                        .show();
            }
        });
    }
}
