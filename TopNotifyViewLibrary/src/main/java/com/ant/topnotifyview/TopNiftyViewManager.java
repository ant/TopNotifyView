//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ant.topnotifyview;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;

class TopNiftyViewManager {
    private static final int MSG_TIMEOUT = 0;
    private static final int SHORT_DURATION_MS = 1500;
    private static final int LONG_DURATION_MS = 2750;
    private static TopNiftyViewManager sSnackbarManager;
    private final Object mLock = new Object();
    private TopNiftyViewManager.SnackbarRecord mCurrentSnackbar;
    private TopNiftyViewManager.SnackbarRecord mNextSnackbar;
    private final Handler mHandler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    TopNiftyViewManager.this.handleTimeout((TopNiftyViewManager.SnackbarRecord) message.obj);
                    return true;
                default:
                    return false;
            }
        }
    });

    private TopNiftyViewManager() {
    }

    static TopNiftyViewManager getInstance() {
        if (sSnackbarManager == null) {
            sSnackbarManager = new TopNiftyViewManager();
        }

        return sSnackbarManager;
    }

    public void show(int duration, TopNiftyViewManager.Callback callback) {
        Object var3 = this.mLock;
        synchronized (this.mLock) {
            if (this.isCurrentSnackbar(callback)) {
                this.mCurrentSnackbar.duration = duration;
                this.mHandler.removeCallbacksAndMessages(this.mCurrentSnackbar);
                this.scheduleTimeoutLocked(this.mCurrentSnackbar);
            } else {
                if (this.isNextSnackbar(callback)) {
                    this.mNextSnackbar.duration = duration;
                } else {
                    this.mNextSnackbar = new TopNiftyViewManager.SnackbarRecord(duration, callback);
                }

                if (this.mCurrentSnackbar == null || !this.cancelSnackbarLocked(this.mCurrentSnackbar)) {
                    this.mCurrentSnackbar = null;
                    this.showNextSnackbarLocked();
                }
            }
        }
    }

    public void dismiss(TopNiftyViewManager.Callback callback) {
        Object var2 = this.mLock;
        synchronized (this.mLock) {
            if (this.isCurrentSnackbar(callback)) {
                this.cancelSnackbarLocked(this.mCurrentSnackbar);
            }

            if (this.isNextSnackbar(callback)) {
                this.cancelSnackbarLocked(this.mNextSnackbar);
            }

        }
    }

    public void onDismissed(TopNiftyViewManager.Callback callback) {
        Object var2 = this.mLock;
        synchronized (this.mLock) {
            if (this.isCurrentSnackbar(callback)) {
                this.mCurrentSnackbar = null;
                if (this.mNextSnackbar != null) {
                    this.showNextSnackbarLocked();
                }
            }

        }
    }

    public void onShown(TopNiftyViewManager.Callback callback) {
        Object var2 = this.mLock;
        synchronized (this.mLock) {
            if (this.isCurrentSnackbar(callback)) {
                this.scheduleTimeoutLocked(this.mCurrentSnackbar);
            }

        }
    }

    public void cancelTimeout(TopNiftyViewManager.Callback callback) {
        Object var2 = this.mLock;
        synchronized (this.mLock) {
            if (this.isCurrentSnackbar(callback)) {
                this.mHandler.removeCallbacksAndMessages(this.mCurrentSnackbar);
            }

        }
    }

    public void restoreTimeout(TopNiftyViewManager.Callback callback) {
        Object var2 = this.mLock;
        synchronized (this.mLock) {
            if (this.isCurrentSnackbar(callback)) {
                this.scheduleTimeoutLocked(this.mCurrentSnackbar);
            }

        }
    }

    private void showNextSnackbarLocked() {
        if (this.mNextSnackbar != null) {
            this.mCurrentSnackbar = this.mNextSnackbar;
            this.mNextSnackbar = null;
            TopNiftyViewManager.Callback callback = (TopNiftyViewManager.Callback) this.mCurrentSnackbar.callback.get();
            if (callback != null) {
                callback.show();
            } else {
                this.mCurrentSnackbar = null;
            }
        }

    }

    private boolean cancelSnackbarLocked(TopNiftyViewManager.SnackbarRecord record) {
        TopNiftyViewManager.Callback callback = (TopNiftyViewManager.Callback) record.callback.get();
        if (callback != null) {
            callback.dismiss();
            return true;
        } else {
            return false;
        }
    }

    private boolean isCurrentSnackbar(TopNiftyViewManager.Callback callback) {
        return this.mCurrentSnackbar != null && this.mCurrentSnackbar.isSnackbar(callback);
    }

    private boolean isNextSnackbar(TopNiftyViewManager.Callback callback) {
        return this.mNextSnackbar != null && this.mNextSnackbar.isSnackbar(callback);
    }

    private void scheduleTimeoutLocked(TopNiftyViewManager.SnackbarRecord r) {
        this.mHandler.removeCallbacksAndMessages(r);
        this.mHandler.sendMessageDelayed(Message.obtain(this.mHandler, 0, r), r.duration == 0 ? 2750L : 1500L);
    }

    private void handleTimeout(TopNiftyViewManager.SnackbarRecord record) {
        Object var2 = this.mLock;
        synchronized (this.mLock) {
            if (this.mCurrentSnackbar == record || this.mNextSnackbar == record) {
                this.cancelSnackbarLocked(record);
            }

        }
    }

    interface Callback {
        void show();

        void dismiss();
    }

    private static class SnackbarRecord {
        private final WeakReference<Callback> callback;
        private int duration;

        SnackbarRecord(int duration, TopNiftyViewManager.Callback callback) {
            this.callback = new WeakReference(callback);
            this.duration = duration;
        }

        boolean isSnackbar(TopNiftyViewManager.Callback callback) {
            return callback != null && this.callback.get() == callback;
        }
    }
}
